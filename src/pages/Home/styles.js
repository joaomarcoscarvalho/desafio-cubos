import styled from "styled-components";

export const Container = styled.main`
  width: 85vw;

  @media (min-width: 1300px) {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;
