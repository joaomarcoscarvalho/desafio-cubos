import styled from "styled-components";

export const Container = styled.section`
  width: 98.9vw;
  /* max-width: 100%; */
  max-width: 1400px;
  margin-top: 3%;
  margin-bottom: 6%;
  div {
    margin-bottom: 3%;
    img {
      width: 100%;
      height: 100%;
    }

    @media (min-width: 700px) {
      display: flex;
      justify-content: space-between;
      img {
        width: 300px;
      }
    }
  }

  p,
  h2 {
    font-family: "Abel", sans-serif;
  }
`;

export const TopTitle = styled.section`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #e6e6e6;
  padding: 20px 3%;

  h2 {
    color: #116193;
    font-size: 2rem;
    font-weight: 200;
  }

  p {
    color: #b0b0b0;
    font-size: 1.2rem;
  }
`;

export const InfoContainer = styled.section`
  padding: 0 3%;
  background-color: #f2f2f2;
  @media (min-width: 700px) {
    width: 90%;
  }
  .subtitle {
    font-size: 1.5rem;
    border-bottom: 2px solid #79edeb;
    padding-top: 3%;
    margin-bottom: 2%;
    padding-bottom: 0.2rem;
    color: #116193;
  }

  .overview {
    font-family: "Lato", sans-serif;
    font-size: 0.9rem;
    color: #636363;
  }
`;

export const InfoData = styled.section`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  /* margin-bottom: 2%;  */
`;

export const Content = styled.section`
  text-align: center;
  margin-bottom: 3%;
  text-transform: capitalize;

  .subtitleData {
    font-size: 1.3rem;
    font-family: "Abel", sans-serif;
    color: #116193;
  }

  p {
    font-family: "Lato", sans-serif;
    font-size: 0.9rem;
    color: #636363;
  }
`;

export const GenresContent = styled.section`
  display: flex;
  flex-wrap: wrap;
  padding-bottom: 3%;
  p {
    padding: 4px 12px;
    border: 2px solid #116193;
    border-radius: 20px;
    font-family: "Abel", sans-serif;
    color: #116193;
    background-color: white;
    margin-right: 2%;
  }
`;

export const Votes = styled.section``;
