import { GET_DETAILS, GET_GENRES, GET_MOVIES, GET_SEARCH } from "./actionsType";

const defaultState = {
  movies: {},
  genres: [],
  details: {},
  search: "",
};

const getMoviesReducer = (state = defaultState, actions) => {
  switch (actions.type) {
    case GET_MOVIES:
      const { movies } = actions;
      return { ...state, movies };
    case GET_SEARCH:
      const { name } = actions;
      return { ...state, search: name };
    case GET_GENRES:
      const { genres } = actions;
      return { ...state, genres };
    case GET_DETAILS:
      const { movie } = actions;
      return { ...state, details: movie };
    default:
      return state;
  }
};

export default getMoviesReducer;
