export const GET_MOVIES = "@GET/movies";
export const GET_SEARCH = "@GET/search";
export const GET_GENRES = "@GET/genres";
export const GET_DETAILS = "@GET/details";
