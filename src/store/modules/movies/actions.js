import { GET_DETAILS, GET_GENRES, GET_MOVIES, GET_SEARCH } from "./actionsType";

export const getMovies = (movies) => ({
  type: GET_MOVIES,
  movies,
});

export const getSearch = (name) => ({
  type: GET_SEARCH,
  name,
});

export const getGenres = (genres) => ({
  type: GET_GENRES,
  genres,
});

export const getDetails = (movie) => ({
  type: GET_DETAILS,
  movie,
});
