import styled from "styled-components";

export const InputBar = styled.section`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: 10%;

  input {
    width: 100%;
    height: 5vh;
    background-color: #ebebeb;
    color: #116193;
    border: none;
    border-radius: 40px;
    padding-left: 10px;
    font-size: 0.7rem;
    outline: none;
    font-family: "Abel", sans-serif;
  }

  input::placeholder {
    color: #116193;
    opacity: 0.5;
  }

  @media (min-width: 481px) {
    input {
      height: 10vh;
      padding-left: 30px;
      font-size: 1.5rem;
    }
  }

  @media (min-width: 769px) {
    margin-top: 6%;
    input {
      font-size: 2rem;
      height: 10vh;
    }
  }

  @media (min-width: 1400px) {
    margin-top: 6%;
    input {
      font-size: 2rem;
      height: 5vh;
    }
  }
`;
