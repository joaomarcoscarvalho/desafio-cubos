import styled from "styled-components";

export const MainHeader = styled.header`
  width: 100vw;
  max-width: 100%;
  height: 10vh;
  background-color: #116193;
  display: flex;
  align-items: center;
  justify-content: center;

  h1 {
    font-family: "Abel", sans-serif;
    font-weight: 100;
    font-size: 2.5rem;
    letter-spacing: 1px;
    color: #00dede;
  }
`;
