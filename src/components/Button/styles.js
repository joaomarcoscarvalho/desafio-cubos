import styled, { css } from "styled-components";

export const ContainerButtons = styled.section`
  width: 100vw;
  max-width: 100%;
  display: flex;
  justify-content: center;

  @media (min-width: 481px) {
    width: 20vw;
    margin-bottom: 2rem;
    margin-top: 2rem;
  }
`;

export const Btn = styled.div`
  font-family: "Abel", sans-serif;

  margin: 4%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 2rem;
  cursor: pointer;
  color: #116193;
  ${({ active }) =>
    active &&
    css`
      margin-left: 5%;
      margin-right: 5%;
      width: 60px;
      height: 60px;
      border-radius: 50%;
      background-color: #116193;
      color: #00dede;
      .button_div_number {
        width: 54px;
        height: 54px;
        display: flex;
        justify-content: center;
        align-items: center;
        border: 3px solid #00dede;
        border-radius: 50%;
        font-size: 2.5rem;
      }
    `};
`;
