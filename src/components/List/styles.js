import styled from "styled-components";

export const ListItem = styled.article`
  display: flex;
  flex-direction: column;
  background-color: #ebebeb;
  margin-top: 10%;
  max-width: 100%;

  .title_popularity {
    display: flex;
    margin-bottom: 1%;
    background-color: #116193;
    padding-top: 4%;
    padding-bottom: 0.5%;
    position: relative;
    cursor: pointer;

    h2 {
      margin-left: 51px;
      color: #00dede;
      font-family: "Abel", sans-serif;
      font-weight: 300;
    }
    .popularity {
      width: 50px;
      height: 50px;
      background-color: #116193;
      border-radius: 50%;
      display: flex;
      justify-content: center;
      align-items: center;
      position: absolute;
      left: 0;
      bottom: -20px;
      color: #00dede;
      font-family: "Abel", sans-serif;

      p {
        width: 46px;
        height: 46px;
        display: flex;
        justify-content: center;
        align-items: center;
        border: 3px solid #00dede;
        border-radius: 50%;
      }
    }
  }

  img {
    width: 100%;
  }

  .release_date {
    margin-bottom: 4%;
    margin-left: 51px;
    font-family: "Abel", sans-serif;
    font-size: 1.3rem;
    color: #b1b1b1;
  }
  .overview {
    font-family: "Lato", sans-serif;
    margin-bottom: 3%;
    margin-right: 4%;
    margin-left: 4%;
    text-align: justify;
    color: #7a7a7a;
  }

  .genres {
    display: flex;
    flex-wrap: wrap;
    margin-left: 4%;
  }

  .genres > p {
    padding: 4px 12px;
    border: 2px solid #116193;
    border-radius: 20px;
    font-family: "Abel", sans-serif;
    color: #116193;
    background-color: white;
    margin-right: 2%;
    margin-bottom: 2%;
  }

  @media (min-width: 769px) {
    flex-direction: row;
    margin-top: 6%;
    .title_popularity {
      .popularity {
        width: 70px;
        height: 70px;
        margin-left: 20px;
        bottom: -32.5px;

        p {
          width: 66px;
          height: 66px;
          font-size: 1.2rem;
          border: 5px solid #00dede;
        }
      }
      h2 {
        font-size: 2rem;
        margin-left: 96px;
      }
    }

    .release_date {
      margin-left: 96px;
    }

    img {
      width: 250px;
    }

    .details {
      width: 100%;
    }
  }

  @media (min-width: 1300px) {
    width: 100%;
  }
`;
