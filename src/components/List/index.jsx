import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { ListItem } from "./styles";

const List = () => {
  const history = useHistory();
  const movies = useSelector((state) => state.movies.movies.movieList);
  const genres = useSelector((state) => state.movies.genres);
  const imageUrl = "https://image.tmdb.org/t/p/w500";

  return (
    <>
      {movies?.map(
        (
          {
            title,
            poster_path,
            overview,
            vote_average,
            release_date,
            id,
            genre_ids,
          },
          index
        ) => (
          <ListItem key={index}>
            <img src={`${imageUrl}${poster_path}`} alt={title} />
            <div className="details">
              <div
                className="title_popularity"
                onClick={() => history.push(`/details/${id}`)}
              >
                <div className="popularity">
                  <p>{parseInt(vote_average * 10)}%</p>
                </div>
                <h2>{title}</h2>
              </div>
              <p className="release_date">
                {release_date?.split("-").reverse().join("/")}
              </p>
              <p className="overview">{overview}</p>
              <div className="genres">
                {genres?.map(
                  (genre, index) =>
                    genre_ids?.indexOf(genre.id) > -1 && (
                      <p key={index}>{genre.name}</p>
                    )
                )}
              </div>
            </div>
          </ListItem>
        )
      )}
    </>
  );
};
export default List;
