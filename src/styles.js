import styled, { createGlobalStyle } from "styled-components";

export const Container = styled.div`
  @import url("https://fonts.googleapis.com/css2?family=Abel&display=swap");
  @import url("https://fonts.googleapis.com/css2?family=Lato&display=swap");
  width: 100vw;
  max-width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const GlobalStyle = createGlobalStyle`
  *{
  box-sizing: border-box;
  padding: 0;
  margin: 0;
  }
`;
